# Project Management Dashboard #
## Description ##
A small project to track my future projects.
The Project Management Dashboard's purpose is to be able to track bugs and features that should be added
to your projects. Users can see who is currently working on features or bugs and the status of these.


## Frontend Technologies Used ##
- Vite
- React
- TypeScript
- React-Router-Dom
- Redux Toolkit
- RTK Query

## Setup / Installation  Backend ##
Clone Repo

Navigate into PMD_FE
Open terminal and run
```
npm i
```
After installing npm packages run
```
npm run dev
``` 

## Contributors ##

**Christoffer Malmberg** @ChristofferMalmberg