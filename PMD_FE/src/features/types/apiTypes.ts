
export type loginCred = {
    id: string,
    password: string
}

export type userResponse = {
    id: number,
    username: string,
    firstName: string,
    lastName: string,
    email: string,
    projects: number[],
    assignedBugs: number[],
    assignedTodoFeatures: number[]
}

export type user = {
    id: number,
    username: string
}

type project = {
    id: number,
    title: string
}

export type newBug = {
    title: string,
    description: string,
    status: string,
    project: {
        id: number,
        title: string
    }
}

export type newFeature = {
    title: string,
    description: string,
    status: string,
    project: {
        id: number,
        title: string
    }
}

export type newProject = {
    title: string,
    creator: user
    description: string,
    url: string,
    privateAccess: boolean
}

export type projectResponse = {
    id: number,
    title: string,
    description: string,
    url: string,
    creator: user
    createdAt: Date,
    lastUpdatedAt: Date,
    bugs: number[],
    features: number[],
    collaborators: user[]
}

export type bugResponse = {
    id: number,
    title: string,
    description: string,
    status: string,
    project: project,
    assignedCollaborators: user[]
}

export type featureResponse = {
    id: number,
    title: string,
    description: string,
    status: string,
    project: project,
    assignedCollaborators: user[]
}