import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { bugResponse, loginCred, projectResponse, featureResponse, userResponse, newProject, newBug, newFeature } from './types/apiTypes'

export const pmdApi = createApi({
    reducerPath: "pmdApi",
    baseQuery: fetchBaseQuery({baseUrl: "http://localhost:8080/api/v1/"}),
    tagTypes: ['CreatedProjects', 'projectBugs', 'projectFeatures'],
    endpoints: (builder) => ({
        getAllUsers: builder.query<userResponse[], void>({
            query: () => "user",
        }),
        getUserById: builder.query<userResponse, number>({
            query: (id) => `user/${id}`,
        }),
        getProjectById: builder.query<projectResponse, number>({
            query: (id) => `project/${id}`,
        }),
        getBugsByProjectId: builder.query<bugResponse[], number>({
            query: (projectId) => `bug/projects/${projectId}`,
            providesTags: ['projectBugs']
        }),
        getUserAssignedBugs: builder.query<bugResponse[], number>({
            query: (userId) => `bug/user/${userId}`
        }),

        getFeaturesByProjectId: builder.query<featureResponse[], number>({
            query: (projectId) => `feature/projects/${projectId}`,
            providesTags: ['projectFeatures']
        }),
        getUserAssignedFeatures: builder.query<featureResponse[], number>({
            query: (userId) => `feature/user/${userId}`
        }),
        getCollaboratorsByProjectId: builder.query<userResponse[], number>({
            query: (projectId) => `user/projects/${projectId}`
        }),
        createProject: builder.mutation<projectResponse, newProject>({
            query: (project) => ({
                url: `project`,
                method: "POST",
                body: project
            }), invalidatesTags: ['CreatedProjects']
        }),
        createBug: builder.mutation<projectResponse, newBug>({
            query: (bug) => ({
                url: `bug`,
                method: "POST",
                body: bug
            }), invalidatesTags: ['projectBugs']
        }),
        createFeature: builder.mutation<featureResponse, newFeature>({
            query: (feature) => ({
                url: `feature`,
                method: "POST",
                body: feature
            }), invalidatesTags: ['projectFeatures']
        }), 
        //TODO check the typing for mutations
        //Move to differnt file later and change to proper auth later
        login: builder.mutation<userResponse, loginCred>({
            query: (credentials) => ({
                url: `user/login`,
                method: "POST",
                body: credentials //This should be changed later
            })
        }),
        getCollabProjects: builder.query<projectResponse[], number>({
            query: (userId) => `project/collab/${userId}`
        }),
        getCreatedProjects: builder.query<projectResponse[], number>({
            query: (userId) => `project/creator/${userId}`,
            providesTags: ['CreatedProjects'],
        }),
    }),
})

export const { useGetAllUsersQuery,
    useGetUserByIdQuery,
    useGetProjectByIdQuery,
    useGetBugsByProjectIdQuery,
    useGetFeaturesByProjectIdQuery,
    useGetCollaboratorsByProjectIdQuery,
    useLoginMutation,
    useCreateProjectMutation,
    useGetCollabProjectsQuery,
    useGetCreatedProjectsQuery,
    useGetUserAssignedBugsQuery,
    useGetUserAssignedFeaturesQuery,
    useCreateBugMutation,
    useCreateFeatureMutation,
    } = pmdApi
