import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../app/store";
import { userResponse } from "./types/apiTypes";

interface credentials {
    user: userResponse | null,
    token: string | null
}

const authSlice = createSlice({
    name: "auth",
    // as credentials sets the type of the initalState obj
    initialState: {user: null, token: null} as credentials,
    reducers: {
        setCredentials: (state, action: PayloadAction<credentials>) => {
            const { user, token } = action.payload
            state.user = user
            state.token = token
        },
        logOut: (state) => {
            state.user = null
            state.token = null 
        }
    }
})

export const { setCredentials, logOut } = authSlice.actions
export default authSlice.reducer