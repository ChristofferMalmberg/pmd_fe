import { Navigate, Outlet } from "react-router-dom"
import { useAppSelector } from "../app/hooks"


const PrivateRoutes = () => {
    const user = useAppSelector((state) => state.auth.user)

    return(
        user ? <Outlet /> : <Navigate to="/" />
    )

}

export default PrivateRoutes