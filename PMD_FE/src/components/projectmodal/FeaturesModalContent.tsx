import { useState } from "react"
import { useGetFeaturesByProjectIdQuery } from "../../features/apiSlice"
import { featureResponse } from "../../features/types/apiTypes"
import FeatureModalForm from "./FeatureModalForm"

type Props = {
    projectId: number
}

const FeaturesModalContent = ({ projectId }: Props) => {

    const [createNewFeature, setCreateNewFeature] = useState<boolean>(false)
    const {data} = useGetFeaturesByProjectIdQuery(projectId)
    const [selectedItem, setSelectedItem] = useState<featureResponse | null>(null)

    return (
        <>
            {createNewFeature ? <button onClick={() => setCreateNewFeature(false)} className="modal-btn">back</button> : <button onClick={() => setCreateNewFeature(true)} className="modal-btn">+</button>}
            {createNewFeature ? <FeatureModalForm projectId={projectId} setCreateNewFeature={setCreateNewFeature} /> : data && !selectedItem ? <div className="modal-list">
                {data.length > 0 ? 
                data.map(feature => {
                    return(
                        <div key={feature.id} className="modal-list-item" onClick={() => setSelectedItem(feature)}>
                            <span>{feature.title}</span>
                            <span>Status: {feature.status}</span>
                            <span>No. Developers Assigned: {feature.assignedCollaborators.length}</span>
                        </div>
                    )
                }) : <h1 style={{textAlign: "center"}}>Currently No New Features Worked On</h1>}
            </div> : <div>
                {/* Spinner */}
                </div>}
            {selectedItem && 
                <div className="selected-item-info">
                    <button onClick={() => setSelectedItem(null)}>back</button>
                    <div className="selected-item-inner">
                        <h1>{selectedItem.title}</h1>
                        <span style={{fontWeight: "bold"}}>Status</span>
                        <span>{selectedItem.status}</span>
                        <span style={{fontWeight: "bold"}}>Description</span>
                        <p>{selectedItem.description}</p>
                        <h2>Developers</h2>
                        <ul className="selected-item-dev-list">
                            {selectedItem.assignedCollaborators.length > 0 ? selectedItem.assignedCollaborators.map(c => <li key={c.id}>{c.username}</li>) : <li>None</li>}
                        </ul>
                    </div>
                </div>}
        </>
    )
}

export default FeaturesModalContent