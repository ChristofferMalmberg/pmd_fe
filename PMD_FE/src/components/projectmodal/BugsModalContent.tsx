import { useState } from "react"
import { useGetBugsByProjectIdQuery } from "../../features/apiSlice"
import { bugResponse } from "../../features/types/apiTypes"
import BugsModalForm from "./BugModalForm"

type Props = {
    projectId: number
}

const BugsModalContent = ({ projectId }: Props) => {

    const {data} = useGetBugsByProjectIdQuery(projectId)
    const [selectedItem, setSelectedItem] = useState<bugResponse | null>(null)
    const [createNewBug, setCreateNewBug] = useState<boolean>(false)

    return (
        <>
            {createNewBug ? <button onClick={() => setCreateNewBug(false)} className="modal-btn">back</button> : <button onClick={() => setCreateNewBug(true)} className="modal-btn">+</button>}
            {createNewBug ? <BugsModalForm setCreateNewBug={setCreateNewBug} projectId={projectId}/> : 
            data && !selectedItem ? <div className="modal-list">
                {data.length > 0 ? 
                data.map(bug => {
                    return(
                        <div key={bug.id} className="modal-list-item" onClick={() => setSelectedItem(bug)}>
                            <span>{bug.title}</span>
                            <span>Status: {bug.status}</span>
                            <span>No. Developers Assigned: {bug.assignedCollaborators.length}</span>
                        </div>
                    )
                }) : <h1 style={{textAlign: "center"}}>Currently No Bugs Reported</h1>}
            </div> : <div>
                    
                </div>}
            {selectedItem && 
                <div className="selected-item-info">
                    <button onClick={() => setSelectedItem(null)}>back</button>
                    <div className="selected-item-inner">
                        <h1>{selectedItem.title}</h1>
                        <span style={{fontWeight: "bold"}}>Status</span>
                        <span>{selectedItem.status}</span>
                        <span style={{fontWeight: "bold"}}>Description</span>
                        <p>{selectedItem.description}</p>
                        <h2>Developers</h2>
                        <ul className="selected-item-dev-list">
                            {selectedItem.assignedCollaborators.length > 0 ? selectedItem.assignedCollaborators.map(c => <li key={c.id}>{c.username}</li>) : <li>None</li>}
                        </ul>
                    </div>
                </div>}
        </>
    )
}

export default BugsModalContent