import { useState } from "react"
import { useGetCollaboratorsByProjectIdQuery } from "../../features/apiSlice"
import { userResponse } from "../../features/types/apiTypes"

type Props = {
    projectId: number
}

const UsersModalContent = ({ projectId }: Props) => {

    const {data} = useGetCollaboratorsByProjectIdQuery(projectId)
    const [selectedItem, setSelectedItem] = useState<userResponse | null>(null)

    return (
        <>
            {data && !selectedItem ? <div className="modal-list">
                {data.length > 0 ? 
                data.map(user => {
                    return(
                        <div key={user.id} className="modal-list-item" onClick={() => setSelectedItem(user)}>
                            <span>{user.username}</span>
                        </div>
                    )
                }) : <h1 style={{textAlign: "center"}}>Currently No Collaborators</h1>}
            </div> : <div>
                {/* spinner */}
                </div>}
            {selectedItem && 
                <div className="selected-item-info">
                    <button onClick={() => setSelectedItem(null)}>back</button>
                    <div className="selected-item-inner">
                        <h1>{selectedItem.username}</h1>
                        <span style={{fontWeight: "bold"}}>First Name</span>
                        <span>{selectedItem.firstName}</span>
                        <span style={{fontWeight: "bold"}}>Last Name</span>
                        <span>{selectedItem.lastName}</span>
                        <span style={{fontWeight: "bold"}}>Email</span>
                        <span>{selectedItem.email}</span>
                    </div>
                </div>}
        </>
    )
}

export default UsersModalContent