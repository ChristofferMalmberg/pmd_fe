import { displayType } from "../../consts/DashItemConst"
import BugsModalContent from "./BugsModalContent"
import FeaturesModalContent from "./FeaturesModalContent"
import UsersModalContent from "./UsersModalContent"


type Props = {
    projectId: number
    displayItem: string | undefined
    compRef: React.RefObject<HTMLDivElement>
    setShowComp: React.Dispatch<React.SetStateAction<boolean>>
}


const Modal = ({ compRef, setShowComp, displayItem, projectId}: Props) => {
    return(
        <>
            <div className="modal-back">
                <div ref={compRef} className="modal-container">
                    <button onClick={() => setShowComp(false)} className="modal-btn">X</button>
                    {displayItem && displayItem == displayType.COLLAB && <UsersModalContent projectId={projectId} /> }
                    {displayItem && displayItem == displayType.FEATURE && <FeaturesModalContent projectId={projectId} /> }
                    {displayItem && displayItem == displayType.BUG && <BugsModalContent projectId={projectId} /> }
                </div>
            </div>
        </>
    )
}

export default Modal