import { useEffect, useState } from "react"
import { FeatureStatuses } from "../../consts/DashItemConst"
import { useCreateFeatureMutation } from "../../features/apiSlice"


type Props = {
    projectId: number,
    setCreateNewFeature: React.Dispatch<React.SetStateAction<boolean>>
}

type InputFields = {
    Title: string,
    Description: string,
    Status: string
}


const FeatureModalForm = ({projectId, setCreateNewFeature}: Props) => {
    
    const [createFeature, {isSuccess}] = useCreateFeatureMutation()

    const [inputFields, setInputsFields] = useState({
        title: "",
        description: "",
        status: FeatureStatuses.PLANNED,
        project: {
            id: projectId,
            title: ""
        }

    })

    const handleFormChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputsFields({...inputFields, [event.target.name]: event.target.value})
    }

    const handleSubmitFeature = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault()
        createFeature(inputFields)
        
    }

    useEffect(() => {
        if(isSuccess) setCreateNewFeature(false)
    }, [isSuccess])


    return(
        <>
            <form className="modal-form">
                <label style={{fontSize: "24px"}}>Title</label>
                <input className="modal-form-title" type="text" required name="title" onChange={(e) => handleFormChange(e)}></input>
                <label style={{fontSize: "24px"}}>Description</label>
                <input className="modal-form-desc" name="description" onChange={(e) => handleFormChange(e)}></input>
                <button onClick={(e) => handleSubmitFeature(e)} className="modal-form-btn">Submit</button>
            </form>
        </>
    )
}

export default FeatureModalForm