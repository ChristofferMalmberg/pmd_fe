import React, { useEffect, useState } from "react"
import { BugStatuses } from "../../consts/DashItemConst"
import { useCreateBugMutation } from "../../features/apiSlice"
import { newBug } from "../../features/types/apiTypes"

type Props = {
    projectId: number,
    setCreateNewBug: React.Dispatch<React.SetStateAction<boolean>>
}

type InputFields = {
    Title: string,
    Description: string,
    Status: string
}

const BugsModalForm = ({ projectId, setCreateNewBug }: Props) => {

    const [createBug, {isSuccess}] = useCreateBugMutation()

    const [inputFields, setInputsFields] = useState<newBug>({
        title: "",
        description: "",
        status: BugStatuses.REPORTED,
        project: {
            id: projectId,
            title: ""
        }

    })

    const handleFormChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputsFields({...inputFields, [event.target.name]: event.target.value})
    }

    const handleSubmitBug = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault()
        createBug(inputFields)
    }

    useEffect(() => {
        if(isSuccess) setCreateNewBug(false)
    }, [isSuccess])


    return(
        <>
            <form className="modal-form">
                <label style={{fontSize: "24px"}}>Title</label>
                <input className="modal-form-title" type="text" required name="title" onChange={(e) => handleFormChange(e)}></input>
                <label style={{fontSize: "24px"}}>Description</label>
                <input className="modal-form-desc" name="description" onChange={(e) => handleFormChange(e)}></input>
                <button onClick={(e) => handleSubmitBug(e)} className="modal-form-btn">Submit</button>
            </form>
        </>
    )
}

export default BugsModalForm