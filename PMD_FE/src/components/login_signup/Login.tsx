import { useLoginMutation } from "../../features/apiSlice"
import React, { useState } from "react"
import { setCredentials } from "../../features/authSlice"
import { userResponse } from "../../features/types/apiTypes"
import { useNavigate } from "react-router-dom"
import { useAppDispatch } from "../../app/hooks"

type inputs = {
    id: string,
    password: string
}

type Props = {
    setSignUp: React.Dispatch<React.SetStateAction<boolean>>
}

const Login = ( { setSignUp }: Props ) => {

    const [inputs, setInputs] = useState<inputs>({id: "", password: ""})
    const [ error, setError ] = useState<boolean>(false)
    const [login, { isLoading }] = useLoginMutation()
    const dispatch = useAppDispatch()
    const navigate = useNavigate()

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        //Spread inputs and replace targeted input field
        setInputs({ ...inputs, [event.target.name]: event.target.value })
    }

    const handleLogin = async (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault() //Prevent the page from reloading
        try {
            //unwraping mutation to access the response/error.
            const userData: userResponse = await login(inputs).unwrap()
            dispatch(setCredentials({ user: userData, token: null}))
            navigate("/dashboard")
        } catch (error) {
            console.log(error)

            //Display error to user
            setError(true)
            setTimeout(() => {
                setError(false)
            }, 3000)
        }
    }

    return(
        <form className="form-container">
            <label>Email</label>
            <input className="form-input" type="text" placeholder="user@user.com" name="id" onChange={handleInputChange}></input>
            <label>Password</label>
            <input className="form-input" type="password" name="password" onChange={handleInputChange}></input>
            <button className="form-btn" onClick={handleLogin}>Log in</button>
            <button className="form-btn" onClick={() => setSignUp(true)}>Sign Up</button>
            {error && <span style={{textAlign: "center", color: "red", fontWeight: "bolder"}}>Error loging in please try again</span>}
        </form>
    )
}

export default Login