import React, { useState } from "react"

type Props = {
    setSignUp: React.Dispatch<React.SetStateAction<boolean>>
}

type InputFieldsAccount = {
    userName: string | null,
    firstName: string | null,
    lastName: string | null,
    email: string | null,
    password: string | null
}

const SignUp = ( { setSignUp }: Props ) => {

    const [inputs, setInputs] = useState<InputFieldsAccount>({
        userName: null,
        firstName: null,
        lastName: null,
        email: null,
        password: null
    })

    const onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInputs({...inputs, [event.target.name]: event.target.value})
    }


    return(
        <form className="form-container">
            <label>Username</label>
            <input className="form-input" type="text" placeholder="LewHam44" name="firstName" onChange={onInputChange}></input>
            <label>First Name</label>
            <input className="form-input" type="text" placeholder="Lewis" name="firstName" onChange={onInputChange}></input>
            <label>Last Name</label>
            <input className="form-input" type="text" placeholder="Hamilton" name="lastName" onChange={onInputChange}></input>
            <label>Email</label>
            <input className="form-input" type="text" placeholder="Lewis@Hamilton.com" name="email" onChange={onInputChange}></input>
            <label>Password</label>
            <input className="form-input" type="password" placeholder="" name="password" onChange={onInputChange}></input>
            <button className="form-btn">Create Account</button>
            <button className="form-btn" onClick={() => setSignUp(false)}>Back</button>
        </form>
    )
}

export default SignUp