import { useNavigate } from "react-router-dom"
import { useAppSelector } from "../app/hooks"


const NavBar = () => {

    const user = useAppSelector((state) => state.auth.user)
    const navigate = useNavigate()

    return(
        <nav className="navbar">
            <ul className="nav-list">
                <li onClick={() => navigate("/")}  className="nav-list-item">Home</li>
                {!user && <li className="nav-list-item">Login</li>}
                {!user && <li className="nav-list-item">Signup</li>}
                {user && <li onClick={() => navigate("/dashboard")} className="nav-list-item">Dashboard</li>}
            </ul>
        </nav>
    )
}

export default NavBar