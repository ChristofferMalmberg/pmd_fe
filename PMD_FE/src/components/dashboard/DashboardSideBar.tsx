import React from "react"
import { DashView } from "../../consts/DashItemConst"

type Props = {
    selectedItem: string,
    setSelected: React.Dispatch<React.SetStateAction<string>>
}

const DashboardSideBar = ( { selectedItem, setSelected }: Props ) => {

    const handleSelected = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
        setSelected(event.currentTarget.id)
    }

    return(
        <div>
            <ul className="sidebar-list">
                <li onClick={handleSelected} id="user" 
                className={selectedItem == DashView.UserDetails ? "selected-sidebar-li" : "sidebar-li"}>
                    User Details
                </li>

                <li onClick={handleSelected} id="project"
                className={selectedItem == DashView.UserProject ? "selected-sidebar-li" : "sidebar-li"}>
                    Projects
                </li>

                <li onClick={handleSelected} id="bug" 
                className={selectedItem == DashView.AssignedBugs ? "selected-sidebar-li" : "sidebar-li"}>
                    Assigned Bugs
                </li>

                <li onClick={handleSelected} id="feat" 
                className={selectedItem == DashView.AssignedFeatures ? "selected-sidebar-li" : "sidebar-li"}>
                    Assigned Features
                </li>
            </ul>
        </div>
    )
}

export default DashboardSideBar