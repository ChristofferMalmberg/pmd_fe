import { useAppSelector } from "../../app/hooks"
import { DashView } from "../../consts/DashItemConst"
import UserBugsList from "./main_content/UserBugsList"
import UserDetails from "./main_content/UserDetails"
import UserFeaturesList from "./main_content/UserFeaturesList"
import UserProjectList from "./main_content/UserProjectList"

type Props = {
    selectedItem: string
}

const DashboardMainContent = ({ selectedItem }: Props) => {

    const user = useAppSelector((state) => state.auth.user)

    return(
        <div>
            {selectedItem.length > 0 ? 
            <div>
                {selectedItem == DashView.UserDetails && <UserDetails />}
                {user && selectedItem == DashView.AssignedBugs && <UserBugsList userId={user.id} />}
                {user && selectedItem == DashView.AssignedFeatures && <UserFeaturesList userId={user.id} />}
                {user && selectedItem == DashView.UserProject && <UserProjectList user={user}/>}
            </div> 
            : 
            <div className="default-text">Please select an item in the sidebar</div>}
        </div>
    )
}

export default DashboardMainContent