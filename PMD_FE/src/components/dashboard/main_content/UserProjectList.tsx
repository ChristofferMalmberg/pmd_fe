import { useNavigate } from "react-router-dom"
import { useShowComp } from "../../../app/hooks"
import { useGetCollabProjectsQuery, useGetCreatedProjectsQuery } from "../../../features/apiSlice"
import { userResponse } from "../../../features/types/apiTypes"
import CreateProjectModal from "./CreateProjectModal"


type Props = {
    user: userResponse
}


const UserProjectList = ({ user }: Props) => {

    const { ref, setShowComp, showComp } = useShowComp()
    const {data: collab} = useGetCollabProjectsQuery(user.id)
    const {data: created} = useGetCreatedProjectsQuery(user.id)
    const navigate = useNavigate()


    return(
        <>
            {showComp && <CreateProjectModal user={user}  compRef={ref} setShowComp={setShowComp}/>}
            <div style={{display: "flex", justifyContent: "end", margin: "1.5rem"}}>
                <button onClick={() => setShowComp(true)}>Create Project</button>
            </div>
            <div className="project-main-container">
                <div className="created-project-container">
                    <h2 style={{textAlign: "center", fontSize: "36px"}}>Created Projects</h2>
                    {created ? created.map(p => {
                        return(<div key={p.id} onClick={() => {navigate("/projects/" + p.title.replaceAll(" ", ""), {state: p})}} className="project-listing">
                            <h1>{p.title}</h1>
                        </div>)
                    }) : <div></div>}
                </div>
                <div className="collab-project-container">
                    <h2 style={{textAlign: "center", fontSize: "36px"}}>Collaboration Projects</h2>
                    {collab ? collab.map(p => {
                        return (<div key={p.id
                        } onClick={() => {navigate("/projects/" + p.title.replaceAll(" ", ""), {state: p})}} className="project-listing">
                            <h1>{p.title}</h1>
                        </div>)
                    }) : <div></div>}
                </div>
            </div>
        </>
    )
}

export default UserProjectList