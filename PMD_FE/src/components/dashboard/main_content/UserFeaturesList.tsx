import { useState } from "react"
import { useShowComp } from "../../../app/hooks"
import { useGetUserAssignedFeaturesQuery } from "../../../features/apiSlice"
import { featureResponse } from "../../../features/types/apiTypes"
import FeatureModal from "./display_item_modal/FeatureModal"

type Props = {
    userId: number
}

const UserFeaturesList = ({ userId }: Props) => {

    const {data} = useGetUserAssignedFeaturesQuery(userId)
    const { ref, setShowComp, showComp } = useShowComp()
    const [ selectedItem, setSelectedItem ] = useState<featureResponse | null>(null)

    const handleItemClick = (feature: featureResponse) => {
        setSelectedItem(feature)
        setShowComp(true)
    }

    return(
        <>
        {showComp && <FeatureModal item={selectedItem} compRef={ref} setShowComp={setShowComp} setSelectedItem={setSelectedItem} />}
         {data ?
              <div>
                {data.length > 0 ? <div>
                    {data.map(feat => {
                        return(
                            <div onClick={() => handleItemClick(feat)} style={{marginTop: "1rem"}} key={feat.id} className="modal-list-item">
                                <span style={{fontWeight: "bold", fontSize: "1.5rem", marginBottom: "10px"}}>{feat.project.title}</span>
                                <span>{feat.title}</span>
                                <span>Status: {feat.status}</span>
                            </div>
                        )
                    })}
                </div> : <h1>User not assigned any bugs</h1>}
              </div> 
            : <div></div>}
        </>
    )
}

export default UserFeaturesList