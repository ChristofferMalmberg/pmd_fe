import { useAppSelector } from "../../../app/hooks"




const UserDetails = () => {

    const user = useAppSelector((state) => state.auth.user)
    return(
        <>
            {user ? <div className="user-details-container">
                <h1 id="user-details-header">User Details</h1>
                <ul className="user-details-list">
                <div className="li-box">
                        <label>Userame:</label>
                        <li className="user-details-li">{user.username}</li>
                    </div>
                    <div className="li-box">
                        <label>First Name:</label>
                        <li className="user-details-li">{user.firstName}</li>
                    </div>
                    <div className="li-box">
                        <label>Last Name:</label>
                        <li className="user-details-li">{user.lastName}</li>
                    </div>
                    <div className="li-box">
                        <label>Email:</label>
                        <li className="user-details-li">{user.email}</li>
                    </div>
                </ul>
            </div> : <h1>No User Info available</h1>}
            
        </>
    )
}

export default UserDetails