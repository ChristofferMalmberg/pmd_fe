import { useState } from "react"
import { useAppSelector } from "../../../app/hooks"
import { useCreateProjectMutation } from "../../../features/apiSlice"
import { user, userResponse } from "../../../features/types/apiTypes"

type Props = {
    user: userResponse,
    compRef: React.RefObject<HTMLDivElement>,
    setShowComp: React.Dispatch<React.SetStateAction<boolean>>
}

type InputFields = {
    title: string,
    creator: user
    description: string,
    url: string,
    privateAccess: boolean
}

//TODO FIX TYPING FOR POST METHOD

const CreateProjectModal = ({ user, compRef, setShowComp }: Props) => {

    const [ inputFields, setInputFields ] = useState<InputFields>({
        title: "",
        creator: {id: user.id, username: user.username},
        description: "",
        url: "",
        privateAccess: false
    })

    const [createProject, { isLoading }] = useCreateProjectMutation()

    const handleInputChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        if(event.target.name == "privateAccess") setInputFields({...inputFields, [event.target.name]: event.target.checked})
        else setInputFields({...inputFields, [event.target.name]: event.target.value})
    }

    const handleCreateProject = () => {
        if(validateFields(inputFields)){
            createProject(inputFields)
        }
        else{

        }
        
    }

    const validateFields = (input: InputFields): boolean => {
        if(input.title.length < 1) return false
        if(input.description.length < 1) return false
        if(input.url.length < 1) return false
        if(input.creator == null) return false
        return true
    }

    return(
        <>
        <div className="modal-back">
                <div ref={compRef} className="modal-container">
                    <button onClick={() => setShowComp(false)} className="modal-btn">X</button>
                    <div className="modal-container-input"> 
                        <h1>Create Project</h1>
                        <div className="modal-input-div">
                            <label className="modal-input-label">Title</label>
                            <input className="modal-input-field" type="text" name="title" onChange={handleInputChanged}/>
                        </div>
                        <div className="modal-input-div">
                            <label className="modal-input-label">Description</label>
                            <input className="modal-input-field" type="text" name="description" onChange={handleInputChanged}/>
                        </div>
                        <div className="modal-input-div">
                            <label className="modal-input-label">Url</label>
                            <input className="modal-input-field" type="text" name="url" onChange={handleInputChanged}/>
                        </div>
                        <div className="modal-input-div-checkbox">
                            <label className="modal-input-label-checkbox">Private Project</label>
                            <input className="modal-input-checkbox" type="checkbox" name="privateAccess" onChange={handleInputChanged}/>
                        </div>
                        <div className="modal-input-div-checkbox">
                            <button onClick={handleCreateProject} style={{alignSelf: "start", padding: "1rem"}}>Create</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CreateProjectModal