import { bugResponse } from "../../../../features/types/apiTypes"

type Props = {
    compRef: React.RefObject<HTMLDivElement>
    item: bugResponse | null
    setShowComp: React.Dispatch<React.SetStateAction<boolean>>
    setSelectedItem: React.Dispatch<React.SetStateAction<bugResponse | null>>
}


const Modal = ({ compRef, setShowComp, item, setSelectedItem }: Props) => {
    return(
        <>
            <div className="modal-back">
                <div ref={compRef} className="modal-container">
                    <button onClick={() => setShowComp(false)} className="modal-btn">X</button>
                    <div>
                    <div className="selected-item-info">
                    <button onClick={() => {setSelectedItem(null), setShowComp(false)}}>back</button>
                    {item && <div className="selected-item-inner">
                        <h1>{item.title}</h1>
                        <span style={{fontWeight: "bold"}}>Status</span>
                        <span>{item.status}</span>
                        <span style={{fontWeight: "bold"}}>Description</span>
                        <p>{item.description}</p>
                        <h2>Developers</h2>
                        <ul className="selected-item-dev-list">
                            {item.assignedCollaborators.length > 0 ? item.assignedCollaborators.map(c => <li key={c.id}>{c.username}</li>) : <li>None</li>}
                        </ul>
                    </div>}
                </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Modal