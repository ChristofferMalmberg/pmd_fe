import { useState } from 'react'
import Login from '../../login_signup/Login'
import SignUp from '../../login_signup/SignUp'

const HomeView = () => {

    const [signUp, setSignUp] = useState<boolean>(false)

    return(
        <>
            <div className="header-box">
                <h1 id="header-text">Project Management App</h1>
                {!signUp ? <Login setSignUp={setSignUp}/> : <SignUp setSignUp={setSignUp}/>}
            </div>
        </>
    )
}

export default HomeView