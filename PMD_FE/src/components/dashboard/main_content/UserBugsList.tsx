import { useState } from "react"
import { useShowComp } from "../../../app/hooks"
import { useGetUserAssignedBugsQuery } from "../../../features/apiSlice"
import { bugResponse } from "../../../features/types/apiTypes"
import Modal from "./display_item_modal/Modal"

type Props = {
    userId: number
}

const UserBugsList = ({ userId }: Props) => {

    const {ref, setShowComp, showComp} = useShowComp()
    const {data} = useGetUserAssignedBugsQuery(userId)
    const [selectedItem, setSelectedItem] = useState<bugResponse | null>(null)

    const handleItemClick = (bug: bugResponse) => {
        setSelectedItem(bug)
        setShowComp(true)
    }

    return(
        <>
            {showComp && <Modal item={selectedItem} compRef={ref} setShowComp={setShowComp} setSelectedItem={setSelectedItem} />}
            {data ?
              <div>
                {data.length > 0 ? <div>
                    {data.map(bug => {
                        return(
                            <div onClick={() => handleItemClick(bug)} style={{marginTop: "1rem"}} key={bug.id} className="modal-list-item">
                                <span style={{fontWeight: "bold", fontSize: "1.5rem", marginBottom: "10px"}}>{bug.project.title}</span>
                                <span>{bug.title}</span>
                                <span>Status: {bug.status}</span>
                            </div>
                        )
                    })}
                </div> : <h1>User not assigned any bugs</h1>}
              </div> 
            : <div></div>}
        </>
    )
}

export default UserBugsList