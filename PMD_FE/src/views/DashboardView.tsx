import { useState } from "react"
import DashboardMainContent from "../components/dashboard/DashboardMainContent"
import DashboardSideBar from "../components/dashboard/DashboardSideBar"


const DashboardView = () => {
    
    const [ selectedSideItem, setSelectedSideItem ] = useState<string>("user")
    
    return(
        <>
            <h1 style={{textAlign: "center", fontSize: "3rem"}}>User Dashboard</h1>
            <div id="dash-container">
                <div className="content-container side-content">
                    <DashboardSideBar selectedItem={selectedSideItem} setSelected={setSelectedSideItem}/>
                </div>
                <div className="content-container main-content">
                    <DashboardMainContent selectedItem={selectedSideItem}/>
                </div>
            </div>
        </>
    )
}

export default DashboardView