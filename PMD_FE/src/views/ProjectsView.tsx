import { useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import { useShowComp } from "../app/hooks"
import Modal from "../components/projectmodal/Modal"
import { displayType } from "../consts/DashItemConst"
import { projectResponse } from "../features/types/apiTypes"


const ProjectsView = () => {

    const { state }: {state: projectResponse} = useLocation()
    const [selectedItem, setSelectedItem] = useState<string>()
    const {ref, showComp, setShowComp} = useShowComp()
    const navigate = useNavigate()

    const handleItemSelect = (event: React.MouseEvent) => {
        console.log(event.currentTarget.id)
        setSelectedItem(event.currentTarget.id)
        setShowComp(true)
    }

    return(
        <>
            <button onClick={() => navigate("/dashboard")} style={{padding: "1rem", margin: "1rem"}}>Back</button>
            <article className="project-page">
                <h1 style={{fontSize: "4rem"}}>{state.title}</h1>
                <section className="project-section">
                    <div onClick={(event) => handleItemSelect(event)} id={displayType.COLLAB}  className="clickable-box grid-item-1">
                        <h3>Current Collaborators</h3>
                        <p style={{textAlign: "center"}}>{state.collaborators.length}</p>
                    </div>
                    <div onClick={(event) => handleItemSelect(event)} id={displayType.FEATURE} className="clickable-box grid-item-2">
                        <h3>Features To Implement</h3>
                        <p style={{textAlign: "center"}}>{state.features.length}</p>
                    </div>
                    <div onClick={(event) => handleItemSelect(event)} id={displayType.BUG} className="clickable-box grid-item-3">
                        <h3>Bugs To Fix</h3>
                        <p style={{textAlign: "center"}}>{state.bugs.length}</p>
                    </div>
                </section>
                <section className="project-section-rows">
                    <h3 style={{fontSize: "2rem"}}>Creator</h3>
                    <span>{state.creator.username}</span>
                    <h3 style={{fontSize: "2rem"}}>Project Description</h3>
                    <p>{state.description}</p>
                </section>
                <section className="project-section-rows" style={{padding: "2rem"}}>
                    <h3>Github / Gitlab</h3>
                    <a className="git-link-btn" href={state.url.startsWith("http://") ? state.url : "http://" + state.url}>To Project</a>
                </section>
                <footer className="project-footer">
                    <span>Last Updated: {state.lastUpdatedAt ? state.lastUpdatedAt.toString() : "unknown"}</span>
                    <span>Creation Date: {state.createdAt ? state.createdAt.toString() : "unknown"}</span>
                </footer>
            </article>
            {showComp && <Modal projectId={state.id} displayItem={selectedItem} compRef={ref} setShowComp={setShowComp}/>}
        </>
    )
}

export default ProjectsView