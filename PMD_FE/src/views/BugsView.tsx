import { useLocation } from "react-router-dom"
import { useGetBugsByProjectIdQuery } from "../features/apiSlice"


const BugsView = () => {

    //Ids of all bugs for a project
    const { state }: { state: number } = useLocation()
    const {data} = useGetBugsByProjectIdQuery(state)

    return(
        <>
            <h1>Bugs View!</h1>
        </>
    )
}

export default BugsView