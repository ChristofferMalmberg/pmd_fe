import { useLocation } from "react-router-dom"
import { useGetFeaturesByProjectIdQuery } from "../features/apiSlice"


const FeaturesView = () => {

    const { state }: {state: number} = useLocation()
    const { data } = useGetFeaturesByProjectIdQuery(state)
    return(
        <>  
            <h1>Features View!</h1>
        </>
    )
}

export default FeaturesView