import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { pmdApi } from '../features/apiSlice'
import authReducer from '../features/authSlice'

export const store = configureStore({
    reducer: {
        [pmdApi.reducerPath]: pmdApi.reducer,
        auth: authReducer,
    },
    middleware: (getDefaultMiddleware) => 
        getDefaultMiddleware().concat(pmdApi.middleware),
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch