import { useDispatch, useSelector } from 'react-redux'
import type { TypedUseSelectorHook } from 'react-redux'
import type { RootState, AppDispatch } from './store'
import { MutableRefObject, RefObject, useEffect, useRef, useState } from 'react'

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
export const useShowComp = () => {
    const [showComp, setShowComp] = useState<boolean>(false)
    const ref  = useRef(null) as RefObject<HTMLDivElement>
    
    useEffect(() => {
        document.addEventListener("click",(event) => handleClickOutside(event), true)
        return () => {
            document.removeEventListener("click",(event) => handleClickOutside(event), false)
        }
    })    

    const handleClickOutside = (event: MouseEvent): void => {
        if(ref.current && !ref.current.contains(event.target as Node)) setShowComp(false)
    }

    return { ref, showComp, setShowComp}
}