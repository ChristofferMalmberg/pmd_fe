
export const DashView = {
    UserProject: "project",
    AssignedBugs: "bug",
    UserDetails: "user",
    AssignedFeatures: "feat"
}

export const displayType = {
    BUG: "bug",
    FEATURE: "feature",
    COLLAB: "collaborators"
}

export const BugStatuses = {
    REPORTED: "REPORTED",
    DEBUGGING: "DEBUGGING",
    SOLVED: "SOLVED"
}

export const FeatureStatuses = {
    PLANNED: "PLANNED",
    IN_DEVELOPMENT: "IN_DEVELOPMENT",
    IMPLEMENTED: "IMPLEMENTED"
}