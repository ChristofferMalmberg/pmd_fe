import { Route, Routes } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import './App.css'
import BugsView from './views/BugsView'
import FeaturesView from './views/FeaturesView'
import HomeView from './components/dashboard/main_content/HomeView'
import DashboardView from './views/DashboardView'
import ProjectsView from './views/ProjectsView'
import NavBar from './components/NavBar'
import PrivateRoutes from './hoc/PrivateRoutes'

function App() {

  return (
    <div className="App">
        <BrowserRouter>
          <NavBar />
          <Routes>
            <Route element={<PrivateRoutes />}>
              <Route path='/dashboard' element={<DashboardView />} />
            </Route>
            <Route path='/' element={<HomeView />} />
            <Route path='/projects/:title' element={<ProjectsView />} />
            {/* path for bugs and features should be /{projectName}/bugs */}
            <Route path='/bugs/project/:title' element={<BugsView />} />
            <Route path='/features/project/:title' element={<FeaturesView />} />
          </Routes>
        </BrowserRouter>
    </div>
  )
}

export default App
